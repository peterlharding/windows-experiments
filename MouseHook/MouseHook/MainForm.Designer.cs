﻿namespace MouseHook
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Rtb_Console = new System.Windows.Forms.RichTextBox();
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Rtb_Console
            // 
            this.Rtb_Console.Location = new System.Drawing.Point(12, 12);
            this.Rtb_Console.Name = "Rtb_Console";
            this.Rtb_Console.ReadOnly = true;
            this.Rtb_Console.Size = new System.Drawing.Size(612, 538);
            this.Rtb_Console.TabIndex = 999;
            this.Rtb_Console.TabStop = false;
            this.Rtb_Console.Text = "";
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Location = new System.Drawing.Point(549, 556);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Exit.TabIndex = 1;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 591);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.Rtb_Console);
            this.Name = "MainForm";
            this.Text = "Mouse Hook Demo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox Rtb_Console;
        private System.Windows.Forms.Button Btn_Exit;
    }
}

