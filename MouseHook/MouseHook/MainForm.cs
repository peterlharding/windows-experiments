﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

// From article - http://stackoverflow.com/questions/11607133/global-mouse-event-handler

namespace MouseHook
{
    //---------------------------------------------------------------------------------------------

    public partial class MainForm : Form
    {
        //-----------------------------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

            MouseHook.Start();
            MouseHook.MouseAction += new EventHandler(MouseHandler);

        }

        //-----------------------------------------------------------------------------------------

        private void MouseHandler(object sender, EventArgs e)
        {
            var d = (DescriptiveEventArgs) e;

            Rtb_Console.AppendText(d.Description);
            Rtb_Console.AppendText(Environment.NewLine);

        } // MouseHandler

        //-----------------------------------------------------------------------------------------

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

    } // class MainForm

    //---------------------------------------------------------------------------------------------

    public class DescriptiveEventArgs : EventArgs
    {
        public string Description { get; set; }

        public DescriptiveEventArgs(string description)
        {
            Description = description;
        }

    } // class DescriptiveEventArgs

    //---------------------------------------------------------------------------------------------

    public static class MouseHook
    {
        public static event EventHandler MouseAction = delegate { };
        private static LowLevelMouseProc _proc = HookCallback;
        private static IntPtr _hookId = IntPtr.Zero;

        private const int WH_MOUSE_LL = 14;

        //-----------------------------------------------------------------------------------------

        private enum MouseMessages
        {
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_MOUSEMOVE = 0x0200,
            WM_MOUSEWHEEL = 0x020A,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205
        }

        //-----------------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int x;
            public int y;
        }

        //-----------------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        private struct MSLLHOOKSTRUCT
        {
            public POINT pt;
            public uint mouseData;
            public uint flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        //-----------------------------------------------------------------------------------------

        public static void Start()
        {
            _hookId = SetHook(_proc);
        } // 

        //-----------------------------------------------------------------------------------------

        public static void Stop()
        {
            UnhookWindowsHookEx(_hookId);
        } // 

        //-----------------------------------------------------------------------------------------

        private static IntPtr SetHook(LowLevelMouseProc proc)
        {
            using (var curProcess = Process.GetCurrentProcess())
            {
                using (var curModule = curProcess.MainModule)
                {
                    var hook = SetWindowsHookEx(WH_MOUSE_LL, proc, GetModuleHandle(curModule.ModuleName), 0);

                    if (hook == IntPtr.Zero) throw new System.ComponentModel.Win32Exception();

                    return hook;
                }
            }

        } // SetHook

        //-----------------------------------------------------------------------------------------

        private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                var hookStruct = (MSLLHOOKSTRUCT) Marshal.PtrToStructure(lParam, typeof (MSLLHOOKSTRUCT));

                var posn = string.Format(@"({0},{1}) {2} {3} {4}", hookStruct.pt.x, hookStruct.pt.y, hookStruct.mouseData, hookStruct.time, hookStruct.flags);

                if (MouseMessages.WM_LBUTTONDOWN == (MouseMessages) wParam)
                {
                    MouseAction(null, new DescriptiveEventArgs(@"Left mouse - " + posn));
                }
                else if (MouseMessages.WM_RBUTTONDOWN == (MouseMessages) wParam)
                {
                    MouseAction(null, new DescriptiveEventArgs("Right mouse - " + posn));
                }
                else if (MouseMessages.WM_MOUSEMOVE == (MouseMessages) wParam)
                {
                    MouseAction(null, new DescriptiveEventArgs("Mouse move - " + posn));
                }
                else if (MouseMessages.WM_MOUSEWHEEL == (MouseMessages) wParam)
                {
                    MouseAction(null, new DescriptiveEventArgs("Mouse wheel - " + posn));
                }
            }

            return CallNextHookEx(_hookId, nCode, wParam, lParam);

        } // HookCallback

        //-----------------------------------------------------------------------------------------

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
          LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
          IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        //-----------------------------------------------------------------------------------------

    }

    //---------------------------------------------------------------------------------------------

}