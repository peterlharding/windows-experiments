﻿using System;
using System.Messaging;
using System.Threading;
using Message_Sender;


namespace MessageQueue_Listener
{
    class Program
    {
        private const string MessageQueueName = @".\private$\Demo";

        //-----------------------------------------------------------------------------------------

        static void Main(string[] args)
        {
            MessageQueue queue = null;

            if (MessageQueue.Exists(MessageQueueName))
            {
                try
                {
                    queue = new MessageQueue(MessageQueueName);

                    Console.WriteLine(@"Using existing queue:");
                    Console.WriteLine(@"       Path: {0}", queue.Path);
                    Console.WriteLine(@" FormatName: {0}", queue.FormatName);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Exception - {0}", ex);
                }
            }
            else
            {
                try
                {
                    queue = MessageQueue.Create(MessageQueueName);

                    queue.Label = @"Demo Queue";

                    Console.WriteLine(@"Created Queue:");
                    Console.WriteLine(@"       Path: {0}", queue.Path);
                    Console.WriteLine(@" FormatName: {0}", queue.FormatName);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Exception - {0}", ex);
                }
            }

            if (queue != null)
            {
                queue.Purge();

                var cnt = 0;
            
                Console.WriteLine(@"cnt -> {0}", cnt);

                while (true)
                {
                    cnt++;

                    Console.WriteLine(@"cnt -> {0}", cnt);

                    if (cnt > 5000) break;

                    Thread.Sleep(50);

                    var message = queue.Receive();

                    if (message != null)
                    {
                        message.Formatter = new XmlMessageFormatter(new [] { typeof(UploadMessage) });

                        var rm = (UploadMessage)message.Body;

                        Console.WriteLine(rm.PdfPath);   
                    }


                }
            }
        } // Main

        //-----------------------------------------------------------------------------------------

    }
}
