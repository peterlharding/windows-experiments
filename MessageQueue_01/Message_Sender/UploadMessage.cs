﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Message_Sender
{
    public class UploadMessage
    {
        public string UserId { get; set; }
        public string PdfPath { get; set; }
    }
}
