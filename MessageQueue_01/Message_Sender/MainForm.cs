﻿using System;
using System.Messaging;
using System.Windows.Forms;
using Message = System.Messaging.Message;

namespace Message_Sender
{
    public partial class MainForm : Form
    {
        private const string MessageQueueName = @".\private$\Demo";
        private MessageQueue _queue;

        //---------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

        } // MainForm

        //---------------------------------------------------------------------

        private void Btn_Main_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();

        } // Btn_Main_Exit_Click

        //---------------------------------------------------------------------

        private void Btn_Main_Send_Click(object sender, EventArgs e)
        {
            Send();

        } // Btn_Main_Send_Click

        //---------------------------------------------------------------------

        private void Send()
        {

            if (MessageQueue.Exists(MessageQueueName))
            {
                _queue = new MessageQueue(MessageQueueName) {Label = "Testing Queue"};
            }
            else
            {
                Console.WriteLine(@"Massage queue {0} not available", MessageQueueName);
                return;
            }

            var noOfMessages = Convert.ToInt32(NoOfMessages.SelectedItem);

            for (var i = 0; i < noOfMessages; i++)
            {
                var message = new Message {Recoverable = true};

                var rm = new UploadMessage
                {
                    UserId = @"user",
                    PdfPath = @"Test_1012346"
                };

                message.Body = rm; // String.Format(@"{0:0000}", _count++);
                message.Formatter = _queue.Formatter;

                _queue.Send(message);
            }

        } // Send

        //---------------------------------------------------------------------

    }

}
