﻿namespace Message_Sender
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Main_Exit = new System.Windows.Forms.Button();
            this.Btn_Main_Send = new System.Windows.Forms.Button();
            this.NoOfMessages = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Btn_Main_Exit
            // 
            this.Btn_Main_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Main_Exit.Location = new System.Drawing.Point(303, 87);
            this.Btn_Main_Exit.Name = "Btn_Main_Exit";
            this.Btn_Main_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Main_Exit.TabIndex = 0;
            this.Btn_Main_Exit.Text = "Exit";
            this.Btn_Main_Exit.UseVisualStyleBackColor = true;
            this.Btn_Main_Exit.Click += new System.EventHandler(this.Btn_Main_Exit_Click);
            // 
            // Btn_Main_Send
            // 
            this.Btn_Main_Send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Main_Send.Location = new System.Drawing.Point(222, 87);
            this.Btn_Main_Send.Name = "Btn_Main_Send";
            this.Btn_Main_Send.Size = new System.Drawing.Size(75, 23);
            this.Btn_Main_Send.TabIndex = 1;
            this.Btn_Main_Send.Text = "Send";
            this.Btn_Main_Send.UseVisualStyleBackColor = true;
            this.Btn_Main_Send.Click += new System.EventHandler(this.Btn_Main_Send_Click);
            // 
            // NoOfMessages
            // 
            this.NoOfMessages.FormattingEnabled = true;
            this.NoOfMessages.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.NoOfMessages.Location = new System.Drawing.Point(12, 12);
            this.NoOfMessages.Name = "NoOfMessages";
            this.NoOfMessages.Size = new System.Drawing.Size(121, 21);
            this.NoOfMessages.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 122);
            this.Controls.Add(this.NoOfMessages);
            this.Controls.Add(this.Btn_Main_Send);
            this.Controls.Add(this.Btn_Main_Exit);
            this.Name = "MainForm";
            this.Text = "Message Sender";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Btn_Main_Exit;
        private System.Windows.Forms.Button Btn_Main_Send;
        private System.Windows.Forms.ComboBox NoOfMessages;
    }
}

