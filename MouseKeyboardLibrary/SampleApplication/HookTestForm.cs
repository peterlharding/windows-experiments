﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MouseKeyboardLibrary;

namespace SampleApplication
{
    public partial class HookTestForm : Form
    {
        readonly MouseHook _mouseHook = new MouseHook();
        readonly KeyboardHook _keyboardHook = new KeyboardHook();

        //-----------------------------------------------------------------------------------------

        public HookTestForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void TestForm_Load(object sender, EventArgs e)
        {

            _mouseHook.MouseMove += mouseHook_MouseMove;
            _mouseHook.MouseDown += mouseHook_MouseDown;
            _mouseHook.MouseUp += mouseHook_MouseUp;
            _mouseHook.MouseWheel += mouseHook_MouseWheel;

            _keyboardHook.KeyDown += keyboardHook_KeyDown;
            _keyboardHook.KeyUp += keyboardHook_KeyUp;
            _keyboardHook.KeyPress += keyboardHook_KeyPress;

            _mouseHook.Start();
            _keyboardHook.Start();

            SetXYLabel(MouseSimulator.X, MouseSimulator.Y);

        }

        //-----------------------------------------------------------------------------------------

        private void keyboardHook_KeyPress(object sender, KeyPressEventArgs e)
        {
            AddKeyboardEvent(
                "KeyPress",
                "",
                e.KeyChar.ToString(),
                "",
                "",
                ""
                );
        }

        //-----------------------------------------------------------------------------------------

        private void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            AddKeyboardEvent(
                "KeyUp",
                e.KeyCode.ToString(),
                "",
                e.Shift.ToString(),
                e.Alt.ToString(),
                e.Control.ToString()
                );
        }

        //-----------------------------------------------------------------------------------------

        private void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            AddKeyboardEvent(
                "KeyDown",
                e.KeyCode.ToString(),
                "",
                e.Shift.ToString(),
                e.Alt.ToString(),
                e.Control.ToString()
                );
        }

        //-----------------------------------------------------------------------------------------

        private void mouseHook_MouseWheel(object sender, MouseEventArgs e)
        {
            AddMouseEvent(
                "MouseWheel",
                "",
                "",
                "",
                e.Delta.ToString()
                );
        }

        //-----------------------------------------------------------------------------------------

        private void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            AddMouseEvent(
                "MouseUp",
                e.Button.ToString(),
                e.X.ToString(),
                e.Y.ToString(),
                ""
                );
        }

        //-----------------------------------------------------------------------------------------

        private void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            AddMouseEvent(
                "MouseDown",
                e.Button.ToString(),
                e.X.ToString(),
                e.Y.ToString(),
                ""
                );
        }

        //-----------------------------------------------------------------------------------------

        private void mouseHook_MouseMove(object sender, MouseEventArgs e)
        {
            SetXYLabel(e.X, e.Y);
        }

        //-----------------------------------------------------------------------------------------

        private void SetXYLabel(int x, int y)
        {
            curXYLabel.Text = String.Format("Current Mouse Point: X={0}, y={1}", x, y);
        }

        //-----------------------------------------------------------------------------------------

        private void AddMouseEvent(string eventType, string button, string x, string y, string delta)
        {
            listView1.Items.Insert(0,
                new ListViewItem(
                    new string[]{
                        eventType, 
                        button,
                        x,
                        y,
                        delta
                    }));
        }

        //-----------------------------------------------------------------------------------------

        private void AddKeyboardEvent(string eventType, string keyCode, string keyChar, string shift, string alt, string control)
        {
            listView2.Items.Insert(0,
                 new ListViewItem(
                     new string[]{
                        eventType, 
                        keyCode,
                        keyChar,
                        shift,
                        alt,
                        control
                }));
        }

        //-----------------------------------------------------------------------------------------

        private void TestForm_FormClosed(object sender, FormClosedEventArgs e)
        {

            // Not necessary anymore, will stop when application exits

            // _mouseHook.Stop();
            // _keyboardHook.Stop();

        }

        //-----------------------------------------------------------------------------------------

    }
}
